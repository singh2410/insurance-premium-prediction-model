#!/usr/bin/env python
# coding: utf-8

# # Insurance Premium Prediction model using sklearn 
# #By- Aarush Kumar
# #Dated: September 07,2021

# In[1]:


import pandas as pd
import numpy as np
import sklearn
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error


# In[2]:


df = pd.read_csv('/home/aarush100616/Downloads/Projects/Insurance Premium Prediction/insurance.csv')


# In[3]:


df


# ## EDA

# In[4]:


df.shape


# In[5]:


df.size


# In[6]:


df.info()


# In[7]:


df.isnull().sum()


# In[8]:


df.dtypes


# In[9]:


df.describe()


# ## Visualization

# In[12]:


import seaborn as sns
import matplotlib.pyplot as plt
plt.subplot(1,1,1)
sns.countplot(x='sex',data=df)


# In[13]:


f = plt.figure(figsize=(10,3))
f.add_subplot(121)
sns.countplot(x='children',data=df)
f.add_subplot(122)
sns.scatterplot(data=df, x="expenses", y="children")


# In[14]:


f = plt.figure(figsize=(10,3))
f.add_subplot(121)
sns.countplot(x='smoker',data=df)
f.add_subplot(122)
sns.scatterplot(data=df, x="expenses", y="smoker")


# In[15]:


sns.countplot(x='region',data=df)


# In[16]:


sns.scatterplot(data=df, x="expenses", y="region")


# In[17]:


f = plt.figure(figsize=(10,3))
f.add_subplot(121)
sns.lineplot(x='age',y='expenses',data=df)
f.add_subplot(122)
sns.lineplot(x='bmi',y='expenses',data=df)


# In[18]:


sns.lineplot(x='bmi',y='expenses',data=df)


# In[19]:


from sklearn.preprocessing import LabelEncoder
label = LabelEncoder()
df.iloc[:,1] = label.fit_transform(df.iloc[:,1])
df.iloc[:,5] = label.fit_transform(df.iloc[:,5])
df.iloc[:,4] = label.fit_transform(df.iloc[:,4])


# In[20]:


import matplotlib.pyplot as plt
corr = df.corr()
# Generate a mask for the upper triangle
mask = np.triu(np.ones_like(corr, dtype=bool))
# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(11, 9))
# Generate a custom diverging colormap
cmap = sns.diverging_palette(230, 20, as_cmap=True)

# Draw the heatmap with the mask and correct aspect ratio
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=.3, center=0,
            square=True, linewidths=.5, cbar_kws={"shrink": .5})


# In[21]:


sns.pairplot(df)


# In[22]:


X = df[['bmi','age','smoker','children']]
Y = df['expenses']


# In[23]:


X_train,X_test,y_train,y_test = sklearn.model_selection.train_test_split(X,Y,test_size=0.25)


# In[24]:


from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import cross_val_score
regressor = DecisionTreeRegressor(random_state=0)
#cross_val_score(regressor, X_train, y_train, cv=10)
regressor.fit(X_train, y_train)
y_predict = regressor.predict(X_test)
mse_dt = mean_squared_error(y_test,y_predict,squared=False)
print(mse_dt)


# In[25]:


from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
#cross_val_score(regressor, X_train, y_train, cv=10)
regressor.fit(X_train, y_train)
y_predict = regressor.predict(X_test)
mse_dt = mean_squared_error(y_test,y_predict,squared=False)
print(mse_dt)


# In[26]:


import pickle
pickle.dump(regressor, open('model.pkl','wb'))

model = pickle.load(open('model.pkl','rb'))


# In[27]:


get_ipython().system('pip install gradio')
import gradio as gr

def greet(bmi,age,smoker,children):
    if smoker:
        is_smoker = 1
    else: 
        is_smoker = 0
    X_test = pd.DataFrame.from_dict({'bmi':[bmi],'age':[age],'smoker':[is_smoker],'children':[children]}) 
    print(X_test)
    y_predict = regressor.predict(X_test)
    print(y_predict)
    return y_predict[0]     

iface = gr.Interface(
  fn=greet, 
  inputs=['text','text','checkbox','text'], 
  outputs="number")
iface.launch(share=True)

